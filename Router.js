import Vue from 'vue'
import Router from 'vue-router'
import NProgress from 'nprogress';
import '../../node_modules/nprogress/nprogress.css'
import { store } from '../store/store'

Vue.use(Router)

const router = new Router({
    mode: 'history',
    routes: [{
            path: '/',
            redirect: { name: 'Login' }
        },
        {
            path: '/login',
            name: 'Login',
            component: () =>
                import ( /* webpackChunkName: "login" */ "@/components/pages/Login")
        },
        {
            path: '/register',
            name: 'Register',
            component: () =>
                import ( /* webpackChunkName: "register" */ "@/components/pages/Register")
        },
        {
            path: '/forgot-password',
            name: 'ForgetPassword',
            component: () =>
                import ( /* webpackChunkName: "reset-password" */ "@/components/pages/ForgetPassword")
        },
        {
            path: '/reset-password/:id',
            name: 'ResetPassword',
            component: () =>
                import ( /* webpackChunkName: "reset-password" */ "@/components/pages/ResetPassword")
        },
        {
            path: '/profile',
            name: 'Profile',
            component: () =>
                import ( /* webpackChunkName: "profile" */ "@/components/pages/Profile")
        },
        {
            path: '/dashboard',
            name: 'Dashboard',
            component: () =>
                import ( /* webpackChunkName: "dashboard" */ "@/components/pages/Dashboard")
        },
        {
            path: '/generate-api',
            name: 'CreateApi',
            component: () =>
                import ( /* webpackChunkName: "generate-api" */ "@/components/pages/CreateApi")
        },
        {
            path: '/generate-api/documentation',
            name: 'CreateApiDoc',
            component: () =>
                import ( /* webpackChunkName: "generate-api-documentation" */ "@/components/pages/CreateApiDoc")
        },
        {
            path: '/analytics',
            name: 'Analytics',
            component: () =>
                import ( /* webpackChunkName: "analytics" */ "@/components/pages/Analytics")
        },
        {
            path: '/live-chat',
            name: 'LiveChat',
            component: () =>
                import ( /* webpackChunkName: "live-chat" */ "@/components/pages/LiveChat")
        },
        {
            path: '/pricing',
            name: 'Pricing',
            component: () =>
                import ( /* webpackChunkName: "pricing" */ "@/components/pages/Pricing")
        },
        {
            path: '/checkout',
            name: 'Checkout',
            component: () =>
                import ( /* webpackChunkName: "checkout" */ "@/components/pages/Checkout")
        },
        {
            path: '*',
            redirect: '/'
        }
    ],

})



router.beforeResolve((to, from, next) => {
    if (to.name) {
        NProgress.start()
        store.commit('routeChangeStatus', true);
    }
    next()
})

router.afterEach((to, from) => {
    NProgress.done()
    setTimeout(() => {
        store.commit('routeChangeStatus', false);
    }, 500);
})



router.beforeEach((to, from, next) => {
    // redirect to login page if not logged in and trying to access a restricted page
    // console.log(to.path, to.params.id);
    const publicPages = ['/login', '/register', '/forgot-password', `/reset-password/${to.params.id}`];
    const authRequired = !publicPages.includes(to.path);
    const loggedIn = localStorage.getItem('user-details');

    if (authRequired && !loggedIn) {
        return next('/login');
    }

    // hide the right side menu on router change
    $('.dropdown-menu.dropdown-menu-right').removeClass('show');
    // on small window left menu toggled off 
    if (document.documentElement.clientWidth < 992) {
        $('nav.sidebar.sidebar-sticky').removeClass('toggled');
    }

    next();
})


export default router