import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export const store = new Vuex.Store({
    state: {
        logginStatus: false,
        publicUserDetails: {},
        routeChangeLoading: false
    },
    mutations: {
        userAuthStatus(state, logginStatus) {
            state.logginStatus = logginStatus;
        },
        userDetails(state, publicUserDetails) {
            state.publicUserDetails = publicUserDetails;
        },
        routeChangeStatus(state, routeChangeLoading) {
            state.routeChangeLoading = routeChangeLoading;
        }
    },
    getters: {}
})